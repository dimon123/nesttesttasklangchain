import { Module } from '@nestjs/common';
import { TextController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [],
  controllers: [TextController],
  providers: [AppService],
})
export class AppModule {}
