import { Controller, Post, Body, BadRequestException } from '@nestjs/common';

@Controller('/process-text')
export class TextController {
  @Post()
  processText(@Body('text') text: string | string[]) {
    try {
      if (!text) {
        throw new BadRequestException('Пустий текст. Введіть текст для обробки.');
      }
  
      let predefinedWords: Record<string, string> = {
        "програмування": "Програмування — процес проєктування, написання, тестування і підтримка комп'ютерних програм.",
        "nest": "Nest - це платформа для створення ефективних масштабованих програм Node.js на стороні сервера.",
        "розумієш": "Частково розумію, але база моїх відповідей сильно обмежена."
      };
  
      // Перетворення рядка на масив слів, якщо text - рядок
      if (typeof text === 'string') {
        text = text.split(/\s+/);
      }
  
      if (text.length > 10) {
        throw new BadRequestException('Забагато тексту. Максимально допустима кількість слів - 10.');
      }
      
      let responce: string[] = [];
      
      // Перебір кожного слова в масиві
      for (let i = 0; i < text.length; i++) {
        let word = text[i];
      
        // Перевірка, чи об'єкт має вказане слово
        if (predefinedWords.hasOwnProperty(word)) {
          console.log(`Знайдено визначення для слова "${word}": ${predefinedWords[word]}`);
          responce.push(predefinedWords[word]);
        } else {
          console.log(`Не знайдено визначення для слова "${word}"`);
          responce.push("Не знаю що вам і сказати");
        }
      }
      
      return `${responce.join(" ")}`;
    } catch (error) {
      return error
    }
   
  }
}
